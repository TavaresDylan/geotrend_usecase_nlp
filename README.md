# Projet UseCase GeoTrend

![Logo Geotrend](https://www.geotrend.fr/wp-content/uploads/2019/07/Logo_seul_geotrend_sans_basline-1024x390.png)

🌐 [Site officiel Geotrend](https://www.geotrend.fr/fr/)

## Objectif du projet 🎯

L'objectif est de **scraper** le site web http://www.trendeo.net/france-septembre-2013-une-economie-a-la-cape/ à l'aide de [BeatifulSoup4](https://www.crummy.com/software/BeautifulSoup/bs4/doc/).

Récupérer toutes les informations et les analyser avec [Spacy](https://spacy.io/)

Générer un fichier **.iob** contenant l'intégralité des expressions.

Ce dernier sert à fine-tuner le modèle existant pour le NLP.

### Ressources

[➤ BIO / IOB Tagged Text to Original Text || Medium.com](https://medium.com/analytics-vidhya/bio-tagged-text-to-original-text-99b05da6664)

[➤ Beautiful Soup and Spacy || Medium.com](https://medium.com/@ichriste/nlp-using-spacy-and-beautiful-soup-82965ea8586a)

[➤ Repo Github de Camille || Github.com](https://github.com/camillepradel/simplon-ner)

[➤ What is a transformer || Medium.com](https://medium.com/inside-machine-learning/what-is-a-transformer-d07dd1fbec04)

[➤ Principe du named entity recognition || Smalsreasearch.be](https://www.smalsresearch.be/named-entity-recognition-une-application-du-nlp-utile/)

# Spacy

[➤ Spacy code snippets || Medium.com](https://medium.com/@ichriste/nlp-using-spacy-and-beautiful-soup-82965ea8586a)

[➤ Spacy Doc NER || Spacy.io](https://spacy.io/usage/linguistic-features#accessing-ner)

[➤ DisplaCy Doc || Spacy.io](https://spacy.io/usage/visualizers)

## C'est quoi le NER ?

Le NER consiste à reconnaître des entités nommées (Named Entities) dans un corpus (ensemble de textes) et de leur attribuer une étiquette telle que “nom”, “lieu”, “date”, “email”, etc. Si le NER désigne au départ l’extraction de noms propres, de noms de lieux et de noms d’organisations, ce concept s’est étendu à d’autres entités telles que la date, l’email, le montant d’argent, etc.

Source => https://www.smalsresearch.be/named-entity-recognition-une-application-du-nlp-utile/

## Import des librairies

Les textes des corpus à analyser sont en français , il va donc falloir importer un des modèles dispo pour la langue française => [Les modèles fr](https://spacy.io/models/fr)

Il y a trois modèles disponibles pour la langue française

- fr_core_news_sm
- fr_core_news_md
- fr_core_news_lg

## C'est quoi IOB ?

- I pour **inside**
- O pour **outside**
- B pour **begin**

## Objectif 

Créer un fichier .iob qui contient l'ensemble des NER d'organisations (ORG)

exemple : 

```txt
Receveur	B-ORG
des	I-ORG
Finances	I-ORG
de	I-ORG
Nantes	I-ORG
Municipale	I-ORG
Préambule	O
```
## Code Snippets

**BeatifulSoup4**

*Startup*

```python
import requests
from bs4 import BeautifulSoup
url = "http://www.trendeo.net/fr/"
page=requests.get(url)
soup = BeautifulSoup(page.content, 'html.parser')
```

*Made By [Dylan Tavarès](https://www.linkedin.com/in/dylan-tavar%C3%A8s-727b23187/) , Jacques Unterlechner with love* ❤️